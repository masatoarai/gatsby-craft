import React, { Component } from 'react'
import gql from 'graphql-tag'
import { ApolloProvider, Query } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'

const httpLink = createHttpLink({
  uri: 'http://gatsby-craft.test/api',
})

const authLink = setContext((_, { headers }) => {
  const token =
    'izpG1-cHr2TvkdiM-qh7iQTY_N8dx-xHHqrR1KcIeW-Rm-miN1PCC_kYm4gwnFjR'
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  }
})

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
})

const QUERY = gql`
  {
    entries(section: [news], limit: 5) {
      ... on News {
        title
        url
        body {
          totalPages
          content
        }
      }
    }
  }
`

client
  .query({
    query: QUERY,
  })
  .then(result => console.log(result))

class IndexPage extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <Query query={QUERY}>
          {({ data, error, loading }) => {
            if (error) return '💩 Oops!'
            if (loading) return 'Loading...'

            return (
              <ul>
                {data.entries.map((entry, i) => (
                  <li key={i}>{entry.title}</li>
                ))}
              </ul>
            )
          }}
        </Query>
      </ApolloProvider>
    )
  }
}

export default IndexPage
